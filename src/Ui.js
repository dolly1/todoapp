import React, { Component } from "react";
import List from "./List"

class Ui extends Component {
  constructor(props) {
  super(props);
  this.createTasks = this.createTasks.bind(this);
  }

  delete(key) {
    this.props.delete(key);
  }

  createTasks(item) {
    return <List itemdata={item}  delete={() => this.delete(item.key)} />
  }

  render() {
    var listItems = this.props.entries.map(this.createTasks);
    return (
      <ul className="theList">
        {listItems}
      </ul>
    );
  }
};

export default Ui;