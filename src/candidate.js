import React from 'react';
import './App.css';


function Formque(props) {
  return (
    <div className="Formque">
    <div>Question = {props.qna.q}</div>
    <div>Answer = {props.qna.a}</div>
    </div>
  );
}


function Candidate(props) {
  return (
    <div className="Candidate">
      <div>name = {props.candidateinfo.full_name}</div>
      <div>email = {props.candidateinfo.email}</div>
      <div>Contact No = {props.candidateinfo.contact_no}</div>
      <div>Branch = {props.candidateinfo.branch}</div>
      <div>College = {props.candidateinfo.college}</div>
    </div>
  );
}

function Candidatedata(props) {
  return (
  <div className="Candidatedata">
  <Candidate candidateinfo={props.candidateinfo} />
  <div>Submission ID = {props.submissionid}</div>
  <div>Form ID = {props.formid}</div>
  <Formque qna={props.qna} />
  </div>
  );
}

 const candidate = {
   "candidateinfo": {
     "full_name": "dolly",
     "email": "dolly@gmail.cm",
     "contact_no": "9925205736",
     "branch": "computer",
     "college": "VVP"
   },
   "submissionid": 3,
   "formid": 1,
   "qna": {
       "a": "php",
       "q": "What is your favourite language?"
     }
 };



let element = <Candidatedata
      candidateinfo={candidate.candidateinfo}
      submissionid={candidate.submissionid}
      formid={candidate.formid}
      qna={candidate.qna}
     />

//element = <div><h1>test</h1></div>

function App() {
  return (
    element
  );
}


export default App;
