import React, { Component } from "react";

class List extends Component {
  constructor(props) {
    console.log(props);
  super(props);
  this.delete = this.delete.bind(this);
  }

  delete() {
    this.props.delete();
  }

  render() {
    return (
      <li key={this.props.itemdata.key} onClick={this.delete}>{this.props.itemdata.text}</li>
    );
  }
};

export default List;