import React, { Component } from "react";

window.id = 0;

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: []
    };
    this.add = this.add.bind(this);
  }

  add(e) {
    var value = this.node.value;
    this.props.add(value);

    // if (value !== "") {
    //   var newItem = {
    //     text: value,
    //     key: window.id++
    //   };

    //   this.setState((prevState) => {
    //     return {
    //       items: prevState.items.concat(newItem)
    //     };
    //   });
    // }

    this.node.value = "";
    e.preventDefault();
  }

  render() {
    return (
    <form onSubmit={this.add}>
      <input ref={(a) => this.node = a} placeholder="enter task">
      </input>
      <button type="submit">add</button>
    </form>
    );
  }
}

export default Form;