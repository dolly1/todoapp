import React, { Component } from "react";
import Ui from "./Ui";
import Form from "./Form";
import "./TodoList.css";
window.id = 0;

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: []
    };

  this.deleteItem = this.deleteItem.bind(this);
  this.addItem = this.addItem.bind(this);
  }

  addItem(value) {
    if (value !== "") {
      var newItem = {
        text: value,
        key: window.id++
      };

      this.setState((prevState) => {
        return {
          items: prevState.items.concat(newItem)
        };
      });
    }
  }

  deleteItem(key) {
    var filteredItems = this.state.items.filter(function (item) {
      return (item.key !== key);
    });
    this.setState({
      items: filteredItems
    });
  }

  render() {
    return (
      <div className="todoListMain">
        <div className="header">
        <Form add={this.addItem}/>
        </div>
        <Ui entries={this.state.items} delete={this.deleteItem}/>
      </div>
    );
  }
}

export default TodoList;